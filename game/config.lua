scenesDir = 'scenes'
scenes = {'game'}

componentsDir = 'components'
components = {'position', 'velocity'}

systemsDir = 'systems'
systems = {'movement'}
