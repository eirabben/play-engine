#version 330 core

layout (location = 0) in vec3 vertPosition;
layout (location = 1) in vec3 vertNormal;
layout (location = 2) in vec2 vertTexCoords;

uniform mat4 camera;
uniform mat4 model;

out vec3 fragPos;
out vec3 fragNormal;
out vec2 fragTexCoords;

void main() {
    gl_Position = camera * model * vec4(vertPosition, 1.0f);
    fragPos = vec3(model * vec4(vertPosition, 1.0f));
    fragNormal = mat3(transpose(inverse(model))) * vertNormal; // Normal matrix should be calculated on the CPU
    fragTexCoords = vertTexCoords;
}
