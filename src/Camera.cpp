#include "Camera.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>

constexpr float maxVerticalAngle = 85.0f; // Avoid gimbal lock

Camera::Camera() {

}

Camera::~Camera() {

}

const glm::vec3& Camera::position() const {
    return mPosition;
}

void Camera::setPosition(const glm::vec3& position) {
    mPosition = position;
}

void Camera::offsetPosition(const glm::vec3& offset) {
    mPosition += offset;
}

float Camera::fieldOfView() const {
    return mFieldOfView;
}

void Camera::setFieldOfView(float fieldOfView) {
    mFieldOfView = fieldOfView;
}

float Camera::nearPlane() const {
    return mNearPlane;
}

float Camera::farPlane() const {
    return mFarPlane;
}

void Camera::setNearAndFarPlanes(float nearPlane, float farPlane) {
    // TODO: assert(nearPlane > 0.0f) and assert(farPlane > nearPlane)
    mNearPlane = nearPlane;
    mFarPlane = farPlane;
}

glm::mat4 Camera::orientation() const {
    glm::mat4 orientation;
    orientation = glm::rotate(orientation, glm::radians(mVerticalAngle), glm::vec3(1, 0, 0));
    orientation = glm::rotate(orientation, glm::radians(mHorizontalAngle), glm::vec3(0, 1, 0));
    return orientation;
}

void Camera::offsetOrientation(float upAngle, float rightAngle) {
    mHorizontalAngle += rightAngle;
    mVerticalAngle += upAngle;
    normalizeAngles();
}

void Camera::lookAt(glm::vec3 position) {
    // TODO: assert(position != mPosition)
    glm::vec3 direction = glm::normalize(position - mPosition);
    mVerticalAngle = glm::radians(std::asin(-direction.y)); // Change this to asinf if it does not work
    mHorizontalAngle = -glm::radians(std::atan2(-direction.x, -direction.z)); // Change to atan2f if error
    normalizeAngles();
}

float Camera::viewportAspectRatio() const {
    return mViewportAspectRatio;
}

void Camera::setViewportAspectRatio(float viewportAspectRatio) {
    // TODO: assert(viewportAspectRatio > 0.0f)
    mViewportAspectRatio = viewportAspectRatio;
}

glm::vec3 Camera::forward() const {
    glm::vec4 forward = glm::inverse(orientation()) * glm::vec4(0, 0, -1, 1);
    return glm::vec3(forward);
}

glm::vec3 Camera::right() const {
    glm::vec4 right = glm::inverse(orientation()) * glm::vec4(1, 0, 0, 1);
    return glm::vec3(right);
}

glm::vec3 Camera::up() const {
    glm::vec4 up = glm::inverse(orientation()) * glm::vec4(0, 1, 0, 1);
    return glm::vec3(up);
}

glm::mat4 Camera::matrix() const {
    return projection() * view();
}

glm::mat4 Camera::projection() const {
    return glm::perspective(glm::radians(mFieldOfView), mViewportAspectRatio, mNearPlane, mFarPlane);
}

glm::mat4 Camera::view() const {
    return orientation() * glm::translate(glm::mat4(), -mPosition);
}

void Camera::normalizeAngles() {
    mHorizontalAngle = std::fmod(mHorizontalAngle, 360.0f);
    if (mHorizontalAngle < 0.0f) {
        mHorizontalAngle += 360.0f;
    }

    if (mVerticalAngle > maxVerticalAngle) {
        mVerticalAngle = maxVerticalAngle;
    } else if (mVerticalAngle < -maxVerticalAngle) {
        mVerticalAngle = -maxVerticalAngle;
    }
}
