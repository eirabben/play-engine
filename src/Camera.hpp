#pragma once
#include <glm/glm.hpp>

class Camera {
public:
    Camera();
    ~Camera();

    // Position
    const glm::vec3& position() const;
    void setPosition(const glm::vec3& position);
    void offsetPosition(const glm::vec3& offset);

    // Field of view
    // Determines how wide or narrow the view is. Must be between 0 and 180.
    float fieldOfView() const;
    void setFieldOfView(float fieldOfView);

    // Near and far plane
    // Objects closer than nearPlane or farther away than farPlane will
    // not be visible.
    float nearPlane() const;
    float farPlane() const;
    void setNearAndFarPlanes(float nearPlane, float farPlane);

    // Rotation matrix
    // Determines the direction the camera is pointing.
    glm::mat4 orientation() const;
    void offsetOrientation(float upAngle, float rightAngle);

    // Orients the camera to look directly at the given position
    void lookAt(glm::vec3 position);

    // Aspect ratio
    float viewportAspectRatio() const;
    void setViewportAspectRatio(float viewportAspectRatio);

    // The direction the camera is facing
    glm::vec3 forward() const;

    // The direction to the right of the camera
    glm::vec3 right() const;

    // The direction out of the top of the camera
    glm::vec3 up() const;

    // The combined camera transformation matrix, including perspective projection
    // This will be used in the vertex shader.
    glm::mat4 matrix() const;

    // Perspective projection transformation matrix
    glm::mat4 projection() const;

    // Translation and rotation matrix of the camera
    // Same as 'matrix', but does not include projection transformation.
    glm::mat4 view() const;

private:
    glm::vec3 mPosition {0.0f, 0.0f, 1.0f};
    float mHorizontalAngle {0.0f};
    float mVerticalAngle {0.0f};
    float mFieldOfView {45.0f};
    float mNearPlane {0.01f};
    float mFarPlane {100.0f};
    float mViewportAspectRatio {4.0f/3.0f};

    void normalizeAngles();
};
