#pragma once

#include "../Ecs/World.hpp"
#include "Position.hpp"
#include "Velocity.hpp"

using World = ecs::World<CPosition, CVelocity>;
// Temporary
using Entity = ecs::Entity<World>;
