#pragma once

struct CPosition {
    CPosition() {}
    CPosition(double x, double y, double z) : x(x), y(y), z(z) {}

    double x;
    double y;
    double z;
};
