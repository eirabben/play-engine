#pragma once

struct CVelocity {
    CVelocity() {}
    CVelocity(double x, double y, double z) : x(x), y(y), z(z) {}

    double x;
    double y;
    double z;
};
