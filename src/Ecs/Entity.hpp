#pragma once

namespace ecs {

template <typename TWorld>
class Entity {
public:
    Entity(TWorld& world, unsigned int id): world(world), id(id) {}
    ~Entity() {}

    template <typename TComponent>
    auto addComponent(TComponent component) {
        world.template addComponent<TComponent>(id, component);
    }

    template <typename TComponent>
    auto& getComponent() {
        return world.template getComponent<TComponent>(id);
    }

    template <typename TComponent>
    auto hasComponent() {
        return world.template hasComponent<TComponent>(id);
    }

    template <typename TComponent>
    auto removeComponent() {
        world.template removeComponent<TComponent>(id);
    }

    auto kill() {
        world.killEntity(id);
    }

    auto getId() const {
        return id;
    }

private:
    unsigned int id;
    TWorld& world;

};

}
