#pragma once

#include <string>
#include <vector>
#include "Bitset.hpp"
#include "ComponentStorage.hpp"
#include "../Script.hpp"
#include "../Components/EcsComponents.hpp"
#include "Entity.hpp"
#include "TypeManager.hpp"

namespace ecs {

constexpr std::size_t defaultCapacity {100};
constexpr std::size_t defaultCapacityIncrease {100};

template <typename... TArgs>
class World {
    using ThisType = World<TArgs...>;
    using Entity = Entity<ThisType>;
public:
    World() {
        resize(defaultCapacity);
    }
    ~World() {}

    // auto createLevel() {
    //     // Load the Lua level file
    //     Script level;
    //     level.load("game.lua");
    //
    //     level.registerClass<CPosition, double, double, double>("Position",
    //         "x", &CPosition::x,
    //         "y", &CPosition::y,
    //         "z", &CPosition::z
    //     );
    //
    //     level.registerClass<CVelocity, double, double, double>("Velocity",
    //         "x", &CVelocity::x,
    //         "y", &CVelocity::y,
    //         "z", &CVelocity::z
    //     );
    //
    //     level.setObject("world", *this,
    //         "create", &World::createEntity
    //     );
    //
    //     level.registerClass<Entity, ThisType, unsigned int>("Entity",
    //         "getId", &Entity::getId,
    //         "addPosition", &Entity::template addComponent<CPosition>,
    //         "getPosition", &Entity::template getComponent<CPosition>,
    //         "hasPosition", &Entity::template hasComponent<CPosition>,
    //         "addVelocity", &Entity::template addComponent<CVelocity>
    //     );
    //
    //     double test = level.callFunction("create");
    //     std::cout << test << std::endl;
    // }

    // auto runSystem() {
    //     Script system;
    //     system.load("system.lua");
    // }

    // Entities
    auto createEntity() {
        resizeIfNeeded(1);
        size++;
        entities.push_back(nextId);
        return getEntity(nextId++);
    }

    auto getEntity(unsigned int id) {
        return Entity {*this, id};
    }

    auto killEntity(unsigned int id) {
        componentStorage.removeEntity(id);
        entitySignatures[id].reset();
        entities.erase(std::remove(entities.begin(), entities.end(), id), entities.end());

        size--;
    }


    // Components
    template <typename TComponent>
    void addComponent(unsigned int id, TComponent component) {
        componentStorage.template add<TComponent>(id, component);

        auto componentType = TypeManager::getTypeFor<TComponent>();
        entitySignatures[id][componentType.bitIndex] = true;
    }

    template <typename TComponent>
    bool hasComponent(unsigned int id) {
        auto componentType = TypeManager::getTypeFor<TComponent>();
        return entitySignatures[id][componentType.bitIndex];
    }

    template <typename TComponent>
    auto& getComponent(unsigned int id) {
       return componentStorage.template getComponent<TComponent>(id);
    }

    template <typename TComponent>
    auto removeComponent(unsigned int id) {
       componentStorage.template removeComponent<TComponent>(id);

       auto componentType = TypeManager::getTypeFor<TComponent>();
       entitySignatures[id][componentType.bitIndex] = false;
    }


    // Signatures
    auto getSignature(unsigned int id) {
        return entitySignatures[id];
    }

    template <typename... TComponents>
    auto matchesSignature(unsigned int id) {
        Bitset signature;
        auto list = {(signature = (signature | TypeManager::getTypeFor<TComponents>().bit))...};
        return (signature & getSignature(id)) == signature;
    }

    template <typename T> struct identity { using type = T; };

    auto forEntities(typename identity<std::function<void(unsigned int)>>::type function) {
        for (auto id : entities) {
            function(id);
        }
    }

    template <typename... TComponents>
    auto forEntitiesMatching(typename identity<std::function<void(unsigned int, TComponents&...)>>::type function) {
        forEntities([this, &function](auto id) {
            if (matchesSignature<TComponents...>(id)) {
                using Helper = ExpandCallHelper<TComponents...>;
                Helper::call(id, *this, function);
            }
        });
    }

    template <typename... Ts>
    struct ExpandCallHelper {
        static void call(unsigned int id, ThisType& world, typename identity<std::function<void(unsigned int entity, Ts&...)>>::type function) {
            function(world.getEntity(id), world.getComponent<Ts>(id)...);
        }
    };


    // Management
    auto resize(std::size_t newCapacity) {
        capacity = newCapacity;
        entities.reserve(capacity);
        entitySignatures.resize(capacity);
        componentStorage.resize(capacity);
    }

    auto resizeIfNeeded(std::size_t newEntities) {
        if (size + newEntities > capacity) {
            resize(capacity + defaultCapacityIncrease);
        }
    }

private:
    std::size_t capacity {0};
    std::size_t size {0};

    unsigned int nextId {0};
    std::vector<unsigned int> entities;
    std::vector<Bitset> entitySignatures;

    ComponentStorage<TArgs...> componentStorage;
};

}
