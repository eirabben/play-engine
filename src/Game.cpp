#include "Game.hpp"
#include <iostream> // Temporary
#include <algorithm>

void Game::run() {
    init();

    // http://gameprogrammingpatterns.com/game-loop.html
    const double msPerUpdate = 1 / 60.0;
    double prevTime = glfwGetTime();
    double lag = 0.0;

    while (!window.shouldClose()) {
        double currentTime = glfwGetTime();
        double elapsed = currentTime - prevTime;
        prevTime = currentTime;
        lag += elapsed;

        handleInput();

        while (lag >= msPerUpdate) {
            update(msPerUpdate);
            lag -= msPerUpdate;
        }
        draw();
    }

    quit();
}

void Game::init() {
    glfwSetErrorCallback(handleError);

    if (!glfwInit()) {
        // TODO: Handle errors
    }

    window.create(1280, 720, "Game"); // TODO: Error checking

    window.setKeyCallback(handleKey);
    window.setCursorPosCallback(handleCursor);
    window.setMouseButtonCallback(handleMouseButton);

    scene.create();
}

void Game::update(double deltaTime) {
    scene.update(deltaTime);
}

void Game::draw() {
    scene.draw();
    window.swapBuffers();
}

void Game::quit() {
    scene.destroy();
    window.destroy();
    glfwTerminate();
}

void Game::handleInput() {
    glfwPollEvents();
}

void Game::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) {

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
}

void Game::handleCursor(GLFWwindow* window, double xpos, double ypos) {
    // std::cout << "Cursor at: " << xpos << ", " << ypos << "\n";
}

void Game::handleMouseButton(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        // Do stuff
    }
}

void Game::handleError(int error, const char* description) {
    std::cout << description << std::endl;
}
