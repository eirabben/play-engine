#pragma once
#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>
#include "Scene.hpp"
#include "Window.hpp"

class Game {
public:
    void run();
    void init();
    void update(double deltaTime);
    void draw();
    void quit();

    void handleInput();

    static void handleError(int error, const char* description);

    static void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void handleCursor(GLFWwindow* window, double xpos, double ypos);
    static void handleMouseButton(GLFWwindow* window, int button, int action, int mods);

private:
    Window window;
    Scene scene {window};

};
