#pragma once
#include <glm/glm.hpp>

struct Light {
    Light(glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular)
        : ambient(ambient), diffuse(diffuse), specular(specular) {}

    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
};

struct DirLight : public Light {
    DirLight(glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular, glm::vec3 direction)
        : Light(ambient, diffuse, specular), direction(direction) {}

    glm::vec3 direction;
};

struct PointLight : public Light {
    PointLight(glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular, glm::vec3 position, float constant, float linear, float quadratic)
        : Light(ambient, diffuse, specular), position(position), constant(constant), linear(linear), quadratic(quadratic) {}

    glm::vec3 position;

    float constant;
    float linear;
    float quadratic;
};
