#pragma once
#include <string>
#include <vector>
#include "Mesh.hpp"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Model {
public:
    Model();
    Model(GLchar* path);
    void loadModel(std::string path);
    void draw(Shader shader);

private:
    std::vector<Texture> mLoadedTextures;
    std::vector<Mesh> mMeshes;
    std::string mDirectory;

    void processNode(aiNode* node, const aiScene* scene);
    Mesh processMesh(aiMesh* mesh, const aiScene* scene);
    std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
};
