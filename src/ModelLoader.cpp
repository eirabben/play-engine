#include "ModelLoader.hpp"

ModelLoader::ModelLoader() {

}
ModelLoader::~ModelLoader() {
    for (auto vao : mVaos) {
        glDeleteVertexArrays(1, &vao);
    }

    for (auto vbo : mVbos) {
        glDeleteBuffers(1, &vbo);
    }
}

void ModelLoader::load(RawModel& model, const std::vector<GLfloat>& data) {
    GLuint vao = createVao();
    bufferData(data);
    unbindVao();
    model.setVao(vao);
    model.setVertexCount((GLint)data.size() / 3);
}

GLuint ModelLoader::createVao() {
    GLuint vao;
    glGenVertexArrays(1, &vao);
    mVaos.push_back(vao);
    glBindVertexArray(vao);
    return vao;
}

void ModelLoader::bufferData(const std::vector<GLfloat>& data) {
    GLuint vbo;
    glGenBuffers(1, &vbo);
    mVbos.push_back(vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(GLfloat), &data[0], GL_STATIC_DRAW);

    // TODO: Make this reuseable by passing in one vector for each attrib array
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ModelLoader::unbindVao() {
    glBindVertexArray(0);
}
