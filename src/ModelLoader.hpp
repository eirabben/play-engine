#pragma once
#include "RawModel.hpp"
#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>
#include <vector>

class ModelLoader {
public:
    ModelLoader();
    ~ModelLoader();

    void load(RawModel& model, const std::vector<GLfloat>& data);

private:
    std::vector<GLuint> mVaos;
    std::vector<GLuint> mVbos;

    GLuint createVao();
    void bufferData(const std::vector<GLfloat>& data);
    void unbindVao();

};
