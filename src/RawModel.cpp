#include "RawModel.hpp"

RawModel::RawModel() {

}

RawModel::RawModel(GLuint vao, GLint vertexCount) : mVao(vao), mVertexCount(vertexCount) {

}

RawModel::~RawModel() {
    
}

GLuint RawModel::getVao() const {
    return mVao;
}

void RawModel::setVao(GLuint vao) {
    mVao = vao;
}

GLint RawModel::getVertexCount() const {
    return mVertexCount;
}

void RawModel::setVertexCount(GLint vertexCount) {
    mVertexCount = vertexCount;
}
