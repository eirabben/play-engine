#pragma once
#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>

class RawModel {
public:
    RawModel();
    RawModel(GLuint vao, GLint vertexCount);
    ~RawModel();

    GLuint getVao() const;
    void setVao(GLuint vao);
    GLint getVertexCount() const;
    void setVertexCount(GLint vertexCount);

private:
    GLuint mVao;
    GLint mVertexCount;
};
