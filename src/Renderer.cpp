#include "Renderer.hpp"

void Renderer::prepare() {
    glClearColor(0.22f, 0.2f, 0.25f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::render(const Model& model, Shader& shader) {
}
