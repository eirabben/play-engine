#pragma once
#include "Model.hpp"

class Renderer {
public:
    void prepare();
    void render(const Model& model, Shader& shader);

private:

};
