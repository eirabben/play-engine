#include "Scene.hpp"
#include <iostream> // Temporary
#include "TextureLoader.hpp"
#include <glm/gtc/matrix_transform.hpp>

Scene::Scene(Window& window) : window(window) {

}

void Scene::create() {
    // Compile shaders
    modelShader.compileShaders("res/Shaders/model_loading.vert", "res/Shaders/model_loading.frag");

    camera.setPosition(glm::vec3(0, 0, 10));
    camera.setViewportAspectRatio(1280.0f / 720.0f);

    for (auto data : lightPositions) {
        PointLight light {
            glm::vec3(0.05f),
            glm::vec3(1.0f),
            glm::vec3(1.0f),
            data,
            1.0f, 0.009f, 0.0032f
        };
        lights.push_back(light);
    }

    model.loadModel("res/nanosuit/nanosuit.obj");

}

bool Scene::update(double deltaTime) {
    const float moveSpeed = 4.0; //units per second
    float delta = deltaTime;
    if(glfwGetKey(window.getWindow(), 'S')){
        camera.offsetPosition(delta * moveSpeed * -camera.forward());
    } else if(glfwGetKey(window.getWindow(), 'W')){
        camera.offsetPosition(delta * moveSpeed * camera.forward());
    }
    if(glfwGetKey(window.getWindow(), 'A')){
        camera.offsetPosition(delta * moveSpeed * -camera.right());
    } else if(glfwGetKey(window.getWindow(), 'D')){
        camera.offsetPosition(delta * moveSpeed * camera.right());
    }
    if(glfwGetKey(window.getWindow(), 'Z')){
        camera.offsetPosition(delta * moveSpeed * -glm::vec3(0,1,0));
    } else if(glfwGetKey(window.getWindow(), 'X')){
        camera.offsetPosition(delta * moveSpeed * glm::vec3(0,1,0));
    }

    //rotate camera based on mouse movement
    const float mouseSensitivity = 0.1f;
    double mouseX, mouseY;
    glfwGetCursorPos(window.getWindow(), &mouseX, &mouseY);
    camera.offsetOrientation(mouseSensitivity * (float)mouseY, mouseSensitivity * (float)mouseX);
    glfwSetCursorPos(window.getWindow(), 0, 0); //reset the mouse, so it doesn't go out of the window

    return true;
}

void Scene::draw() {
    renderer.prepare();

    // RENDER OBJECT

    modelShader.use();

    for (int i = 0; i < lights.size(); i++) {
        auto light = lights[i];
        modelShader.loadUniform(modelShader.getUniformLocation("pointLights[" + std::to_string(i) + "].position"), light.position);
        modelShader.loadUniform(modelShader.getUniformLocation("pointLights[" + std::to_string(i) + "].ambient"), light.ambient);
        modelShader.loadUniform(modelShader.getUniformLocation("pointLights[" + std::to_string(i) + "].diffuse"), light.diffuse);
        modelShader.loadUniform(modelShader.getUniformLocation("pointLights[" + std::to_string(i) + "].specular"), light.specular);
        modelShader.loadUniform(modelShader.getUniformLocation("pointLights[" + std::to_string(i) + "].constant"), light.constant);
        modelShader.loadUniform(modelShader.getUniformLocation("pointLights[" + std::to_string(i) + "].linear"), light.linear);
        modelShader.loadUniform(modelShader.getUniformLocation("pointLights[" + std::to_string(i) + "].quadratic"), light.quadratic);
    }

    modelShader.loadUniform(modelShader.getUniformLocation("camera"), camera.matrix());

    glm::mat4 modelMat;
    modelMat = glm::translate(modelMat, glm::vec3(0.0f, -1.75f, 0.0f));
    modelMat = glm::scale(modelMat, glm::vec3(0.2f, 0.2f, 0.2f));
    modelShader.loadUniform(modelShader.getUniformLocation("model"), modelMat);

    model.draw(modelShader);

    modelShader.unuse();
}

void Scene::destroy() {
}
