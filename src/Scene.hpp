#pragma once
#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <vector>
#include "Shader.hpp"
#include "Model.hpp"
#include "Light.hpp"
#include "Camera.hpp"
#include "Window.hpp"
#include "Components/Components.hpp"
#include "Renderer.hpp"

class Scene {
public:
    Scene(Window& window);

    void create();

    bool update(double deltaTime);
    void draw();

    void destroy();

private:
    bool quit {false};

    Window& window;
    Shader modelShader;
    Camera camera;
    Model model;
    Renderer renderer;
    std::vector<PointLight> lights;

    std::vector<glm::vec3> lightPositions {
        glm::vec3(2.3f, -1.6f, -3.0f),
        glm::vec3(-1.7f, 0.9f, 1.0f)
    };
};
