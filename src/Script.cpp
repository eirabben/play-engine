#include "Script.hpp"

void Script::load(std::string filename) {
    state.Load(filename);
}

std::string Script::getString(std::string name) {
    std::string var = state[name.c_str()];
    return var;
}

int Script::getInt(std::string name) {
    int var = state[name.c_str()];
    return var;
}

double Script::getDouble(std::string name) {
    double var = state[name.c_str()];
    return var;
}

float Script::getFloat(std::string name) {
    double dVar = state[name.c_str()];
    float var = dVar;
    return var;
}

bool Script::getBool(std::string name) {
    bool var = state[name.c_str()];
    return var;
}
