#pragma once

#include <string>
#include <functional>
#include <Selene.h>

class Script {
public:
    void load(std::string filename);

    std::string getString(std::string name);
    int getInt(std::string name);
    double getDouble(std::string name);
    float getFloat(std::string name);
    bool getBool(std::string name);

    template <typename... TArgs>
    auto callFunction(std::string name, TArgs... args) {
        return state[name.c_str()](args...);
    }

    template <typename T, typename... CtorArgs, typename... Funs>
    void registerClass(std::string name, Funs... funs) {
        state[name.c_str()].SetClass<T, CtorArgs...>(funs...);
    }

    template <typename T, typename... Funs>
    void setObject(std::string name, T& object, Funs... funs) {
        state[name.c_str()].SetObj(object, funs...);
    }

private:
    sel::State state;

};
