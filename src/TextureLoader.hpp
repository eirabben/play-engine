#pragma once
#include "Mesh.hpp"
#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>
#include <string>

class TextureLoader {
public:
    static Texture load(std::string filePath);
    static GLuint TextureFromFile(const char* path, std::string directory);
};
