#include "Window.hpp"

bool Window::create(int width, int height, std::string title) {
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    mWindow = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (!mWindow) {
        return false;
    }

    glfwMakeContextCurrent(mWindow);
    glfwSwapInterval(1);

    glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPos(mWindow, 0, 0);

    int bufferWidth, bufferHeight;
    glfwGetFramebufferSize(mWindow, &bufferWidth, &bufferHeight);
    glViewport(0, 0, bufferWidth, bufferHeight);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    return true;
}

void Window::destroy() {
    glfwDestroyWindow(mWindow);
}

void Window::swapBuffers() {
    glfwSwapBuffers(mWindow);
}

bool Window::shouldClose() {
    return glfwWindowShouldClose(mWindow);
}

GLFWwindow* Window::getWindow() const {
    return mWindow;
}

void Window::setKeyCallback(GLFWkeyfun callback) {
    glfwSetKeyCallback(mWindow, callback);
}

void Window::setCursorPosCallback(GLFWcursorposfun callback) {
    glfwSetCursorPosCallback(mWindow, callback);
}

void Window::setMouseButtonCallback(GLFWmousebuttonfun callback) {
    glfwSetMouseButtonCallback(mWindow, callback);
}
