#pragma once

#include <GLFW/glfw3.h>
#include <string>

class Window {
public:
    bool create(int width, int height, std::string title);
    void destroy();

    void swapBuffers();

    bool shouldClose();

    GLFWwindow* getWindow() const;

    void setKeyCallback(GLFWkeyfun callback);
    void setCursorPosCallback(GLFWcursorposfun callback);
    void setMouseButtonCallback(GLFWmousebuttonfun callback);

private:
    GLFWwindow* mWindow;

};
